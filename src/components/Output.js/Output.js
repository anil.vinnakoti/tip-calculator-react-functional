import React from 'react'
import Person from './Person';
import Reset from './Reset';

const Output = (props) => {
  const {billPerPerson, tipPerPerson} = props.values

  const styles={
    width:'49%',
    padding:'1.5rem',
    borderRadius:'0.6rem',
    display:'flex',
    flexDirection:'column',
    backgroundColor: 'hsl(183, 100%, 15%)',
    color:'hsl(0, 0%, 100%)',
    fontSize:'0.9rem'
}

return (
    <div style={styles}>
      <Person content = 'Tip Amount' amount={tipPerPerson} />
      <Person content = 'Total' amount={billPerPerson} />
      <Reset resetFn={props.resetFn} />
    </div>
);
}

export default Output