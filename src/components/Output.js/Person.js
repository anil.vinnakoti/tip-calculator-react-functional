import React from 'react'

const Person = (props) => {

  const stylesMain = {
    display:'flex',
    justifyContent:'space-between',
    marginBottom:'20px',
  }

  const innerStyles = {
      display:'flex',
      flexDirection:'column'
  }
  const colorForTitles ={
    color:'hsl(172, 67%, 45%)'
  }

return (
    <div style={stylesMain}>
        <div style={innerStyles}>
            <span>{props.content}</span>
            <span style={colorForTitles}>/ person</span>
        </div>

        <div>
            $ {!Number.isInteger(props.amount) ? 0 : props.amount}
        </div>
    </div>
);
}

export default Person