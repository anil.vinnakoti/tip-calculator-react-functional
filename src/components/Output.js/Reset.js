import React from 'react'

const Reset = (props) => {
  return (
      <div className='reset-button'>
          <button onClick={props.resetFn}>RESET</button>
      </div>
  )
}

export default Reset