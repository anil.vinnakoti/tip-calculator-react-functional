import React from 'react'
import Input from './input/Input'
import Output from './Output.js/Output'

function Calculator(props) {
  
  return (
    <div className='container' style={{display:'flex', backgroundColor:'white', borderRadius:'20px', padding:'3rem'}}>
      <Input 
        values={props.values} 
        billHandler={props.billHandler} 
        peopleHandler={props.peopleHandler} 
        tipHandler={props.tipHandler} 
        tipFormHandler={props.tipFormHandler} />

      <Output values={props.values} resetFn = {props.resetFn} />
    </div>
  )
}

export default Calculator