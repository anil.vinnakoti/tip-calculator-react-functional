import React from 'react'
import TipButton from './TipButton'

const Tip = (props) => {
  const percentages = [5,10,15,25,50];  
  
  return (
    <div onClick={props.handler}>
      <label>Tip %</label>
      <div>
        {percentages.map((item,index) => <TipButton key={index} content={item} item={item}  ></TipButton>)}
      </div>
      <input type='text' placeholder='Custom %' value={props.values.percentage} onChange={props.tipFormHandler}/>
    </div>
  )
}

export default Tip