import React from 'react'

const TipButton = (props) => {
  return (
    <>
      <button style={{marginRight:'0.4rem'}} data-tip={props.item}>{props.item}%</button>
    </>
  )
}

export default TipButton