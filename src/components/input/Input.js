import React from 'react'
import Form from './Form'
import Tip from './Tip'

const Input = (props) => {

  return (
    <div>
      <Form valueProp={props.values.billAmount} handler={props.billHandler} content='Bill Amount' />
      <Tip values={props.values} handler={props.tipHandler} tipFormHandler={props.tipFormHandler} />
      <Form valueProp = {props.values.people} handler={props.peopleHandler} content='Number of People'/>
    </div>
  )
}

export default Input