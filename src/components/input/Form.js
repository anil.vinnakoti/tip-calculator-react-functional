import React from 'react'

const Form = (props) => {
  
  return (
    <form >
      <label>{props.content}</label>
      <input type='text'
      placeholder={props.placeHolder} 
      value={props.valueProp} 
      onChange={props.handler} />
    </form>
  )

}

export default Form