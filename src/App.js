import React, {useEffect, useState} from 'react';
import './App.css';
import Calculator from './components/Calculator';

function App() {

  const styles = {
    height:'100vh',
    display: 'flex',
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor:'hsl(185, 41%, 84%'
  }

  const state = {
    billAmount: 0,
    percentage: 0,
    people: 0,
    tipPerPerson:0,
    billPerPerson:0
}



const [values, setValues] = useState(state)

  const handleBillInput = event => {
    if(parseFloat(event.target.value)){
    setValues({...values, billAmount:parseFloat(event.target.value)}) 
    }else{
      setValues({...values, billAmount:0 })
    }
  }

  const handleTipInput = event => {
      setValues({...values, percentage:parseFloat(event.target.dataset.tip)});
  }

  const handleTipForm = event =>{
    setValues({...values, percentage:parseFloat(event.target.value)})
  }

  const handlePeopleInput = event => {
    if(parseFloat(event.target.value)){
      setValues({...values, people:parseFloat(event.target.value)})
    }else{
      setValues({...values, people:0 })
    };
  }

  const reset = event => {
    setValues({...values, 
      billAmount:0, 
      percentage:0,
      people:0,
      tipPerPerson:0,
      billPerPerson:0
    })
  }

  useEffect(() => {

    const calculate = () => {
      const {billAmount, percentage, people} = values
      let tipPercentage = percentage/100

      let tip = ((billAmount*tipPercentage)/people)
      let bill = ((billAmount/people)+tip)

      setValues({...values, tipPerPerson:tip, billPerPerson:bill})
    }

    calculate()

  },[values.billAmount,values.percentage,values.people])

  return (
    <div style={styles} className="App">
      <Calculator 
      values={values} 
      billHandler = {handleBillInput} 
      peopleHandler={handlePeopleInput} 
      tipHandler = {handleTipInput}
      tipFormHandler = {handleTipForm}
      resetFn = {reset}
      />
    </div>
  );
}

export default App;
